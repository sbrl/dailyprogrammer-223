if(process.argv.length < 3)
{
	console.log("Usage:\n\tnode enable_checker.js <substring>");
	process.exit();
}
var substring = process.argv[2];

//////////////////////

var fs = require("fs"),
	
	enable1 = fs.readFileSync("enable1.txt", "utf-8").split("\r\n");

// By /u/r2vq
function check_word(word, substring)
{
	return word.split("").filter(function(char) { 
		return substring.indexOf(char) >= 0
	}).join("") === substring;
}

function check_against_enable1(substring)
{
	var count = 0;
	for(word of enable1)
	{
		if(check_word(word, substring)) count++;
	}
	return count;
}

console.log(check_against_enable1(substring));