// #13 transmap - 120 bytes
function transmap(s,m){return s.split("").map(function(c){if(typeof m[c]=="string")return m[c];else return c}).join("")}
// #15 transmapupdate - 86 bytes
function transmapupdate(a,b,m){[].forEach.call(a,function(c,i) { m[c]=b[i]});return m}

function *substring_engine(start, block_size)
{
	"use strict";
	var i = typeof start == "number" ? start : 0,
		next = "aaaaa",
		
		map = {};
	
	if(typeof block_size !== "number")
		block_size = 1;
	
	transmapupdate("0123456789abcdefghijklmnop",
				  "abcdefghijklmnopqrstuvwxyz", map);
	
	do
	{
		next = ("aaaa" + transmap(i.toString(26), map)).slice(-5);
		
		let cmd = yield next;
		if(cmd === true) // Exit if we are told to
			return;
		
		i+= block_size;
	} while(next !== "zzzzz");
}

/*** Old generator ***
function *substring_engine(start)
{
	"use strict";
	var state = typeof q === "undefined" ? "aaaaa".split("") : start,
		next = "aaaaa";
	do
	{
		for(let i = state.length - 1; i >= 0; i--)
		{
			if(state[i] == "z")
			{
				state[i] = "a";
				continue;
			}
//			console.log(state.length, state, i, state[i]);
			state[i] = String.fromCharCode(state[i].charCodeAt(0) + 1);
			break;
		}
		next = state.join("");
		let end = yield next;
		if(end === true) return;
	} while(next !== "zzzzz");
}
**********************/

/*
var iterator = substring_engine(100, 16),
	next = iterator.next(), i = 0;

do
{
//	if(i % 100000 == 0) process.stderr.write(next.value + "\n");
	console.log(next.value);
	next = iterator.next();
	i++;
} while(!next.done && i < 200);
*/


module.exports = substring_engine;
