console.log("Substring Checking Server");
console.log("=-=-=-=-=-=-=-=-=-=-=-=-=");

if(process.argv.length < 3)
{
	console.log("Usage:\n\tnode server.js <port>");
	process.exit();
}

var port = parseInt(process.argv[2]);

/// Other Settings ///

// The number of substrings clients process at once.
var block_size = 16,
	// The maximum amount of time we should wait for a block to be completed
	// before reallocating it in seconds
	max_processing_time = block_size * 5, // 5 seconds per substring, loads of time
	// The size of the leaderboard
	leaderboard_size = 20;

//////////////////////

console.log("block size:", block_size);
console.log("maximum processing time:", max_processing_time);
console.log("leaderboard size:", leaderboard_size);

function get_time() { return new Date().toLocaleTimeString(); }
var http = require("http"),
	fs = require("fs"),
	WebSocketServer = require("websocket").server,
	substring_engine = require("./generator.js"),
	
	http_server = http.createServer(function(req, resp) {
		resp.writeHead(200);
		resp.end("Word Checker Server");
	}).listen(port, function() {
		console.log(`${get_time()} server listening on port ${port}`);
	}),
	
	ws_server = new WebSocketServer({
		httpServer: http_server,
		autoAcceptConnections: false
	}),
	connections = [];


function *block_machine(block_size)
{
	var max = 11881375, // number of possibilities
		i = 0;
	
	do
	{
//		console.log(`factory state: i: ${i}, block size: ${block_size}, max: ${max}`);
		
		yield i;
		i += block_size;
	} while(i < max);
}

var block_factory = block_machine(block_size),
	// array to record blocks we are reallocating
	blocks_processing = 0,
	last_block = 0,
	prev_last_block = 0,
	blocks_per_second = 0,
	incomplete_blocks = [], 
	leaderboard = [];

// Calculate the blocks per second regularly
setInterval(function() {
	blocks_per_second = ((last_block - prev_last_block) / block_size) / 10;
	prev_last_block = last_block;
}, 10 * 1000);

for(var i = 0; i < leaderboard_size; i++)
{
	leaderboard.push({ name: "undefined", count: 0 });
}

function send_block(connection)
{
	var next_block;
	if(incomplete_blocks.length == 0)
		next_block = block_factory.next();
	else
		next_block = { value: incomplete_blocks.shift(), done: false };
	
	last_block = next_block.value;
	
	if(!next_block.done)
	{
		console.log(`${get_time()} Sending block ${next_block.value} to ${connection.remoteAddress}`);
		connection.send(JSON.stringify({
			event: "process_block",
			start: next_block.value
		}));
	}
	else
	{
		if(blocks_processing == 0)
		{
			console.log(`${get_time()} All blocks processed!`);
		}
		else
		{
			console.log(`${get_time()} Holding allocation request from ${connection.remoteAddress} as there aren't any more blocks left to allocate! Waiting for ${blocks_processing} more blocks to finish processing...`);
			setTimeOut(function() {
				send_block(connection);
			}, (max_processing_time / 10) * 1000);
		}
	}
	
	return setTimeout(function() {
		console.log(`${get_time()} Filing block ${next_block.value} for reallocation`);
		incomplete_blocks.push(next_block.value);
	}, max_processing_time * 1000);
}

function get_lowest_count()
{
	return leaderboard[leaderboard.length - 1].count;
}

function broadcast_min_count()
{
	var min_count = get_lowest_count();
	
	for(var i = 0; i < connections.length; i++)
	{
		connections[i].send(JSON.stringify({
			event: "min_count",
			min_count: min_count
		}));
	}
}

function get_stats()
{
	return {
		event: "stats",
		leaderboard: leaderboard,
		peers: connections.length,
		blocks_per_second: blocks_per_second,
		reallocation_queue_size: incomplete_blocks.length,
		last_block_allocated: last_block,
		max_block: Math.ceil(11881375 / block_size)
	};
}

setInterval(function() {
	fs.writeFileSync("stats.json", JSON.stringify(get_stats()));
}, 60 * 1000); // Every minute

// websocket message handler
ws_server.on("request", function(request) {
	var connection = request.accept("", request.origin); // NEVER do this in production
	connections.push(connection);
	
	var block_reallocation;
	
	console.log(get_time() + " connection accepted from " + connection.remoteAddress);
	
	connection.on("message", function(message) {
		try {
			var msg = JSON.parse(message.utf8Data);
		}
		catch(error) {
			console.log(`${get_time()} Invalid message from ${connection.remoteAddress}`);
			return;
		}
		
		try {
			switch(msg.event)
			{
				case "block_complete":
					clearTimeout(block_reallocation); // Cancel the automatic reallocation
					console.log(`${get_time()} Block ${msg.start} completed by ${connection.remoteAddress}`);
					
	//				console.log(msg.results);
					
					Array.prototype.push.apply(leaderboard, msg.result);
					leaderboard.sort(function(a, b) {
						if(a.count > b.count) return -1;
						if(a.count < b.count) return +1;
						return 0;
					});
					
					// Limit the size of the leaderboard
					leaderboard.length = leaderboard_size;
					
					broadcast_min_count(); // Tell everyone about the new min count
					
					block_reallocation = send_block(connection);
					break;
					
				case "stats":
					connection.send(JSON.stringify(get_stats()));
					break;
			}
		} catch (error) {
			console.log(`${get_time()} Error processing message from ${connection.remoteAddress}:`);
			// Semi-production:
			console.log(error);
			// Dev:
//			throw error;
		}
	});
	connection.on("close", function(code, description) {
		console.log(`${new Date()} ${connection.remoteAddress} disconnected (${code} - ${description}`);
		// Remove the connection from the connections array
		for(var i = 0; i < connections.length; i++)
		{
			if(connections[i] == connection) connections.splice(i, 1);
		}
	});
	
	connection.send(JSON.stringify({
		event: "block_size",
		block_size: block_size
	}));
	block_reallocation = send_block(connection);
});