if(process.argv.length < 4)
{
	console.log("Usage:\n\tiojs checker.js <word> <substring>");
	process.exit();
}
var word = process.argv[2],
	substring = process.argv[3];

// By /u/r2vq
function check_word(word, substring)
{
	return word.split("").filter(function(char) { 
		return substring.indexOf(char) >= 0
	}).join("") === substring;
}

console.log(check_word(word, substring));