console.log("Substring Checking Client");
console.log("=-=-=-=-=-=-=-=-=-=-=-=-=");

if(process.argv.length < 3)
{
	console.log("Usage:\n\tnode client.js <ip:port>");
}
var server_address = process.argv[2];

/// Other Settings ///

// Be very verbose when logging things.
var verbose = false,

// The interval at which we should request the statistics.
	stats_interval = 10 * 1000; // Every 10 seconds

//////////////////////

function get_time() { return new Date().toLocaleTimeString(); }

var fs = require("fs"),
	WebSocketClient = require("websocket").client,
	substring_engine = require("./generator.js");

console.log(`${get_time()} Reading in enable1.txt...`);
var enable1 = fs.readFileSync("enable1.txt", "utf-8").split("\r\n");
console.log(`${get_time()} ...done`);

// By /u/r2vq
function check_word(word, substring)
{
	return word.split("").filter(function(char) { 
		return substring.indexOf(char) >= 0
	}).join("") === substring;
}

function check_against_enable1(substring)
{
	var count = 0;
	for(word of enable1)
	{
		if(check_word(word, substring)) count++;
	}
	if(verbose)
	{
		console.log(`${get_time()} Processed ${substring} ( count: ${count} )`);
	}
	return count;
}

var ws_client = new WebSocketClient(),
	block_size = 1, // The number of substrings to process after the number we are given
	min_count = 0; // The minimum count to bother about

ws_client.on('connectFailed', function(err) {
	console.log(`${get_time()} Connect error: ${err.toString()}`);
});

ws_client.on("connect", function(connection) {
	console.log(`${get_time()} Connected to ${server_address}.`);
	
	connection.on("error", function(err) {
		console.error(`${get_time()} Connection error: ${err.toString()}`);
	});
	connection.on("close", function() {
		console.log(`${get_time()} Connection to ${server_address} lost.`);
		process.exit();
	});
	connection.on("message", function(message) {
		"use strict";
//		console.log(`${get_time()} Got message: `, message);
		var msg = JSON.parse(message.utf8Data);
		switch(msg.event)
		{
			case "block_size": // update our block size
				block_size = msg.block_size;
				console.log(`${get_time()} Set block size to ${block_size}`);
				break;
			
			case "min_count":
				min_count = msg.min_count;
				break;
			
			case "stats":
				console.log(`${get_time()} Stats: `);
				console.log(`Peers: ${msg.peers}, Blocks per second: ${msg.blocks_per_second}, Reallocation queue size: ${msg.reallocation_queue_size}`);
				var percent_complete = ((msg.last_block_allocated / block_size) / msg.max_block) * 100;
				console.log(`Progress: ${msg.last_block_allocated / block_size} / ${msg.max_block} blocks complete ( ${percent_complete}% )`);
				
				for(let entry of msg.leaderboard)
				{
					console.log(`${entry.name}: ${entry.count}`);
				}
				
				break;
			
			case "process_block":
				console.log(`${get_time()} Processing block ${msg.start} ( size: ${block_size} )`);
				var results = [],
					iterator = substring_engine(msg.start),
					next = iterator.next();
				
				for(let i = 0; i < block_size; i++)
				{
					let result = check_against_enable1(next.value);
					if(result > 0)
						results.push({
							name: next.value,
							count: result
						});
					
					next = iterator.next();
					if(next.done == true) break;
				}
				iterator.next(true);
				
//				console.log(results);
				
				connection.sendUTF(JSON.stringify({
					event: "block_complete",
					start: msg.start,
					block_size: block_size, // just in case we update this later...?
					result: results
				}));
				
				break;
		}
	});
	
	setInterval(function() {
		connection.send(JSON.stringify({
			event: "stats"
		}));
	}, stats_interval);
});

ws_client.connect(`ws://${server_address}/`, "");