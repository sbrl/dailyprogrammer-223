# /r/dailyprogrammer Challenge #223 Optional Challenge #2

Hi! This is my first distributed computing attempt. It solves [/r/dailyprgrammer](https://www.reddit.com/r/dailyprogrammer)'s [Challenge #223](https://www.reddit.com/r/dailyprogrammer/comments/3ddpms/20150715_challenge_223_intermediate_eel_of_fortune/) using [/u/r2vq](https://reddit.com/u/r2vq)'s [solution](https://www.reddit.com/r/dailyprogrammer/comments/3ddpms/20150715_challenge_223_intermediate_eel_of_fortune/ct4zdyh).

This solution contains 2 main parts. The _client_ and the _server_. The client does the actual work, and the server co-ordinatees the work by handing out blocks. Both the server and client utilise some ES6 features, so make sure you have a recent version of io.js (Node.JS should work too, but I haven't tested it you will probably need to use `node --harmony` instead of just `node`).

(maybe incomplete) List of ES6 features used:

 - [String Interpolation](http://es6-features.org/#StringInterpolation)
 - [for..of](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Statements/for...of)
 - [Generators](https://starbeamrainbowlabs.com/blog/article.php?article=posts/80-Javascript-Generators.html)
 - 

## Getting Started
1. Open your favourite console / terminal
2. Clone this repository
3. `cd` into the repository's directory
4. Install the npm module `websocket` by either typing `npm install` or `npm install websocket`.
5. Follow the instructions below to start a client and / or server.

## Start a server:

```bash
~$ node server.js 4321
```

Where `9999` is the port number.

## Connect a client:
```bash
~$ node client.js 192.168.1.56:4321
```

Where the address to connect to is in the form `ip:port`. The client is single threaded, so start as many clients as you want CPUs to be used.

The client will automatically display the current server stats every 10 seconds. You can change this value in `client.js`.

### Connect to the public server!
**I am currently running a version of this server open to the public. Here are the details you need to connect:**

 - **IP Address:** 37.187.192.179
 - **Port:** 9999
 - **Connect command:** `node client.js 37.187.192.179:9999`

If you are going to change the interval at which stats are displayed, be aware that the client requests the stats from the server every time. Please be considerate and don't set it any lower than 5 seconds.